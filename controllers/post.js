const {succesResponse}  = require("../helper/response.js")
const fs = require('fs');
const { stringify } = require("querystring");

class Post {
    constructor() {
        this.post = []
        const readpost =fs.readFileSync('./data/post.json','utf-8');
        if (readpost == ""){
            this.parsedPost = this.post;
        } else{
        this.parsedPost =JSON.parse(readpost)
        }
    }

    getPost = (req, res) =>{
        
        succesResponse(
            res,
            200,
            this.parsedPost,
            {total: this.parsedPost.length}
        )
    }

    getDetailPost =(req, res)=>{
        const index = req.params.index
       succesResponse(res, 200, parsedPost[index])
    }  

    insertPost =(req, res)=>{
        const body = req.body
        
        const param = {
            'text': body.text,
            'created at': new Date(),
            'username': body.username
        }
        console.log(this.parsedPost)
        this.parsedPost.push(param)
        fs.writeFileSync('./data/post.json', JSON.stringify(this.parsedPost))
        succesResponse(res,201,param)
    }

    updatePost =(req, res)=>{
        const index = req.params.index
        const body = req.body
    if (this.parsedPost[index]){
        this.parsedPost[index].text = body.text
        this.parsedPost[index].username = body.username
        fs.writeFileSync('./data/post.json', JSON.stringify(this.parsedPost))
        succesResponse(res,200, this.parsedPost[index])
    }else
    succesResponse(res,422, null)
        
    }

    deletePost =(req, res)=>{
        const index = req.params.index
        
        this.post.splice(index, 1)

        succesResponse(res, 200, null)

    }

}

module.exports = Post