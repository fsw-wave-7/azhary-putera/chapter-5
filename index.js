const express = require('express')
const app = express()
const port = 3000
const Post = require('./controllers/post.js')
const api = require('./routes/api.js')
const path = require('path')


app.set('view engine','ejs')

app.use(express.static(__dirname + '/public'));


app.use('/api',api)
// })
app.get('/menu', function(req, res){
    res.render(path.join(__dirname, './views/index'),{
   content: './pages/menu'
    })
})

app.get('/', function(req, res) {
    const name = req.query.name || 'Starbucks'
    res.render(path.join(__dirname, './views/index'), {
        name: name,
        content: './pages/home'
    })
})



const post = new Post
app.get('/', function(req, res){
    res.json({
        'hello': 'world'
    })
})

app.get('/testLOG', function(req, res){
    res.json({
        'check': 'log'
    })
})




app.listen(port, () => {console.log("server berhasil dijalankan")})

